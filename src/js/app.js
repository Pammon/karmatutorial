(function(window) {

	"use strict"
	
	var app = {};

	app.greet = function(name) {
		if(name == null)return "hello Anonymous";
		else return "hello " + name;
	};

	window.APP = app;

})(this);